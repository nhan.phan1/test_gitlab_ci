package com.ninjax.weather.ui.base

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

/**
 * Created by nmduc3 on 6/1/20
 */
open class BaseAdapterPager(
    fragment: Fragment,
    private val parentLevel: Int,
    private val fragments: MutableList<Fragment>
) : FragmentStateAdapter(fragment) {

    override fun getItemCount() = fragments.size

    override fun createFragment(position: Int): Fragment {
        val fragment = fragments[position]
        (fragment as? BaseFragment<*>)?.setLevel(parentLevel + 1)
        return fragment
    }
}
