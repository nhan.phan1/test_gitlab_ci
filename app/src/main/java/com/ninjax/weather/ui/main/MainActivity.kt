package com.ninjax.weather.ui.main

import com.ninjax.weather.R
import com.ninjax.weather.ui.base.BaseActivity
import com.ninjax.weather.ui.container.ContainerFragment
import com.ninjax.weather.ui.main.MainFragment

class MainActivity : BaseActivity() {
    override fun getLayoutResource() = R.layout.activity_main

    override fun mainContainer() = MainFragment()

    override fun superContainer() = ContainerFragment()
}
