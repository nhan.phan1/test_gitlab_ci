package com.ninjax.weather.ui.base

import android.os.Bundle
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import com.ninjax.weather.R
import com.ninjax.weather.util.Constants

abstract class FragmentController : Fragment() {
    internal var level: Int = 0
        private set

    internal fun setLevel(level: Int) {
        this.level = level
    }

    private var callBackWhenBackPress: OnBackPressedCallback = object : OnBackPressedCallback(
        false
        /** true means that the callback is enabled */
    ) {
        override fun handleOnBackPressed() {
            handleBackPressed()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // note that you could enable/disable the callback here as well by setting callback.isEnabled = true/false
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            callBackWhenBackPress
        )
    }

    private fun handleAddCallBack(isEnable: Boolean = true) {
        callBackWhenBackPress.isEnabled = isEnable
    }

    override fun onPause() {
        super.onPause()
        handleAddCallBack(false)
    }

    protected open fun handleBackPressed() {
        when (level) {
            Constants.Level.TOP, Constants.Level.CONTAINER -> return
            Constants.Level.TAB -> {
                parentFragmentManager.popBackStack()
            }
            else -> {
                if (level % 2 == 0) {
                    // child fragment in viewpager
                    parentFragmentManager.also {
                        if (it.backStackEntryCount > 0) {
                            // pop in child viewpager
                            it.popBackStack()
                        } else {
                            // child in viewpager size == 0
                            // pop in parent
                            (parentFragment as? FragmentController)?.handleBackPressed()
                        }
                    }

                } else {
                    // container
                    (parentFragment as? FragmentController)?.handleBackPressed()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        handleAddCallBack(true)
    }

    internal fun replaceFragment(
        fragment: Fragment, isAddBackStack: Boolean,
        isEnableAnim: Boolean = true, tagNameBackStack: String? = null
    ) {
        val range = level - 1
        when (level) {
            // do not use fragment manager of activity
            Constants.Level.TOP -> return
            Constants.Level.CONTAINER -> {
                // add in main tab folow
                replaceInContainer(
                    fragment,
                    isAddBackStack,
                    isEnableAnim,
                    tagNameBackStack
                )
            }
            else -> {
                // get fragment manager of fragment when level = 1(Main top container)
                var parentFm: Fragment? = this
                for (index in 1..range) {
                    parentFm = parentFm?.parentFragment
                }
                // user fragment with level = 1 to add fragment
                (parentFm as? FragmentController)?.replaceInContainer(
                    fragment,
                    isAddBackStack,
                    isEnableAnim,
                    tagNameBackStack
                )
            }
        }
    }

    /**
     * add fragment in main tab follow
     */

    internal fun addFragment(
        fragment: Fragment,
        isEnableAnim: Boolean = true,
        tagNameBackStack: String? = null
    ) {
        val range = level - 1
        when (level) {
            // do not use fragment manager of activity
            Constants.Level.TOP -> return
            Constants.Level.CONTAINER -> {
                // add in main tab follow
                addInContainer(
                    fragment,
                    isEnableAnim,
                    tagNameBackStack
                )
            }
            else -> {
                // get fragment manager of fragment when level = 1(Main top container)
                var parentFm: Fragment? = this
                for (index in 1..range) {
                    parentFm = parentFm?.parentFragment
                }
                // user fragment with level = 1 to add fragment
                (parentFm as? FragmentController)?.addInContainer(
                    fragment,
                    isEnableAnim,
                    tagNameBackStack
                )
            }
        }
    }

    /**
     * add fragment in viewpager
     */
    internal fun addInChildFragment(
        fragment: Fragment,
        isEnableAnim: Boolean = true,
        tagNameBackStack: String? = null
    ) {
        when {
            // do not use fragment manager of activity
            level == Constants.Level.TOP -> return
            level == Constants.Level.CONTAINER || level % 2 != 0 -> {
                // in this base: container level is a odd number
                addInContainer(
                    fragment,
                    isEnableAnim,
                    tagNameBackStack
                )
            }
            level == Constants.Level.TAB || level % 2 == 0 -> {
                // child in viewpager
                (parentFragment as? FragmentController)?.addInContainer(
                    fragment,
                    isEnableAnim,
                    tagNameBackStack
                )
            }
            else -> return
        }
    }

    /**
     * replace fragment in viewpager
     */
    internal fun replaceInChildFragment(
        fragment: Fragment, isAddBackStack: Boolean,
        isEnableAnim: Boolean = true, tagNameBackStack: String? = null
    ) {
        when {
            // do not use fragment manager of activity
            level == Constants.Level.TOP -> return
            level == Constants.Level.CONTAINER || level % 2 != 0 -> {
                // in this base: container level is a odd number
                replaceInContainer(
                    fragment,
                    isAddBackStack,
                    isEnableAnim,
                    tagNameBackStack
                )
            }
            level == Constants.Level.TAB || level % 2 == 0 -> {
                // child in viewpager
                (parentFragment as? FragmentController)?.replaceInContainer(
                    fragment,
                    isAddBackStack,
                    isEnableAnim,
                    tagNameBackStack
                )
            }
            else -> return
        }
    }

    internal fun replaceInContainer(
        fragment: Fragment, isAddBackStack: Boolean,
        isEnableAnim: Boolean = true, tagNameBackStack: String? = null
    ) {
        (fragment as? FragmentController)?.setLevel(level + 1)
        childFragmentManager.beginTransaction().apply {
            if (isEnableAnim) {
                setCustomAnimations(
                    R.anim.slide_in_right, R.anim.slide_out_left,
                    R.anim.slide_in_left, R.anim.slide_out_right
                )
            }
            replace(R.id.container, fragment, fragment.javaClass.simpleName)
            if (isAddBackStack) {
                addToBackStack(tagNameBackStack)
            }
            commit()
        }
    }

    internal fun addInContainer(
        fragment: Fragment,
        isEnableAnim: Boolean = true, tagNameBackStack: String? = null
    ) {
        (fragment as? FragmentController)?.setLevel(level + 1)
        childFragmentManager.beginTransaction().apply {
            if (isEnableAnim) {
                setCustomAnimations(
                    R.anim.slide_in_right, R.anim.slide_out_left,
                    R.anim.slide_in_left, R.anim.slide_out_right
                )
            }
            add(R.id.container, fragment, fragment.javaClass.simpleName)
            addToBackStack(tagNameBackStack)
            commit()
        }
    }

    internal fun addSuperFragment(
        fragment: Fragment,
        isEnableAnim: Boolean = true, tagNameBackStack: String? = null
    ) {
        (activity as? BaseActivity)?.addSuperFragment(
            fragment,
            isEnableAnim,
            tagNameBackStack
        )
    }
}
