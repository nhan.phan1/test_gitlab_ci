package com.ninjax.weather.ui.container

import com.ninjax.weather.R
import com.ninjax.weather.ui.base.BaseFragment
import com.ninjax.weather.viewmodel.BaseViewModel

/**
 * Created by nmduc3 on 5/29/20
 */
class ContainerFragment : BaseFragment<BaseViewModel>() {
    override fun getLayoutResource(): Int = R.layout.fragment_container

    override fun viewModel(): BaseViewModel = BaseViewModel()
}
