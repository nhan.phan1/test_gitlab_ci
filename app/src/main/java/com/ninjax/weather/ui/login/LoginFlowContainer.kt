package com.ninjax.weather.ui.login

import android.os.Bundle
import android.view.View
import com.ninjax.weather.R
import com.ninjax.weather.ui.base.BaseFragment
import com.ninjax.weather.ui.login.termofservice.TermOfServiceFragment
import com.ninjax.weather.viewmodel.BaseViewModel

/**
 * Created by nmduc3 on 5/29/20
 */
class LoginFlowContainer : BaseFragment<BaseViewModel>() {
    override fun getLayoutResource(): Int = R.layout.fragment_container

    override fun viewModel() = BaseViewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        replaceFragment(TermOfServiceFragment(), false)
    }
}
