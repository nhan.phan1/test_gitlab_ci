package com.ninjax.weather.ui.login.termofservice

import android.os.Bundle
import android.view.View
import com.ninjax.weather.R
import com.ninjax.weather.ui.base.BaseFragment
import com.ninjax.weather.ui.login.usernameandpassword.UsernameAndPasswordFragment
import kotlinx.android.synthetic.main.fragment_term_of_service.*
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * Created by nmduc3 on 5/29/20
 */
class TermOfServiceFragment : BaseFragment<TermOfServiceViewModel>() {
    override fun getLayoutResource() = R.layout.fragment_term_of_service

    override fun viewModel() = viewModel<TermOfServiceViewModel>().value

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvAgree.setOnClickListener {
            replaceFragment(UsernameAndPasswordFragment(), true)
        }
    }
}
