package com.ninjax.weather.ui.main.home

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.ninjax.weather.R
import com.ninjax.weather.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * Created by nmduc3 on 6/1/20
 */
class HomeFragment : BaseFragment<HomeViewModel>() {
    companion object {
        private const val KEY_POSITION = "key_position"

        fun newInstance(position: Int) = HomeFragment().apply {
            arguments = Bundle().apply {
                putInt(KEY_POSITION, position)
            }
        }
    }

    override fun getLayoutResource() = R.layout.fragment_home

    override fun viewModel() = viewModel<HomeViewModel>().value

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Init views
        initViews()

    }

    private fun initViews() {
        tvHomeTitle.text = "Page: ${arguments?.getInt(KEY_POSITION)} -> Level: $level"
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        // handle result from API
        viewModel().getWeatherResult().observe(viewLifecycleOwner, Observer { result ->
            tvWeather.text = result.name
        })
    }
}
