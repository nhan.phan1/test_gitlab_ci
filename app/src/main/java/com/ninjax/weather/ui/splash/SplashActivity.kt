package com.ninjax.weather.ui.splash

import com.ninjax.weather.R
import com.ninjax.weather.ui.base.BaseActivity
import com.ninjax.weather.util.Constants

/**
 * Created by nmduc3 on 5/29/20
 */
class SplashActivity : BaseActivity() {
    override fun getLayoutResource() = R.layout.activity_main

    override fun mainContainer() = SplashFragment().apply {
        setLevel(Constants.Level.CONTAINER)
    }
}
