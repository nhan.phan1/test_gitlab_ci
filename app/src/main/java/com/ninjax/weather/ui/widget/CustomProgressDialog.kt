package com.ninjax.weather.ui.widget

import android.app.Dialog
import android.content.Context
import android.view.Window
import com.ninjax.weather.R

/**
 * @author at-hoahoang
 */
class CustomProgressDialog(context: Context) : Dialog(context) {

    init {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_custom_progress)
        window?.run {
            setDimAmount(0f)
            setBackgroundDrawableResource(android.R.color.transparent)
        }
        setCancelable(false)
    }
}
