package com.ninjax.weather.ui.main

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.ninjax.weather.R
import com.ninjax.weather.ui.base.BaseAdapterPager
import com.ninjax.weather.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : BaseFragment<MainViewModel>() {
    private val fragments = mutableListOf<Fragment>()

    private val pagerAdapter: BaseAdapterPager by lazy {
        BaseAdapterPager(this, level, fragments)
    }

    override fun viewModel(): MainViewModel = viewModel<MainViewModel>().value

    override fun getLayoutResource(): Int = R.layout.fragment_main

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        fragments.clear()
        EMainPager.values().forEachIndexed { index, _ ->
            fragments.add(MainPagerContainerFragment.newInstance(index))
        }
    }

    private fun initViews() {
        vpMain.adapter = pagerAdapter
        bottomNavigation.run {
            setOnNavigationItemSelectedListener {
                vpMain.currentItem = it.order
                true
            }
        }
    }
}
