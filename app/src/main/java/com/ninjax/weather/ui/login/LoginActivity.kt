package com.ninjax.weather.ui.login

import com.ninjax.weather.R
import com.ninjax.weather.ui.base.BaseActivity
import com.ninjax.weather.util.Constants

/**
 * Created by nmduc3 on 5/29/20
 */
class LoginActivity : BaseActivity() {
    override fun getLayoutResource() = R.layout.activity_main

    override fun mainContainer() = LoginFlowContainer().apply {
        setLevel(Constants.Level.CONTAINER)
    }
}
