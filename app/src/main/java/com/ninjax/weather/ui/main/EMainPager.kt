package com.ninjax.weather.ui.main

/**
 * Created by nmduc3 on 6/1/20
 */
enum class EMainPager {
    HOME,
    SEARCH,
    FAVOURITES,
    CALENDAR
}
