package com.ninjax.weather.ui.splash

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.ninjax.weather.R
import com.ninjax.weather.ui.base.BaseFragment
import com.ninjax.weather.ui.login.LoginActivity
import com.ninjax.weather.ui.main.MainActivity
import com.ninjax.weather.viewmodel.BaseViewModel
import kotlinx.android.synthetic.main.fragment_splash.*

/**
 * Created by nmduc3 on 5/29/20
 */
class SplashFragment : BaseFragment<BaseViewModel>() {
    override fun getLayoutResource() = R.layout.fragment_splash

    override fun viewModel() = BaseViewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvLogin.setOnClickListener {
            startActivity(Intent(context, LoginActivity::class.java))
        }
        tvHome.setOnClickListener {
            startActivity(Intent(context, MainActivity::class.java))
        }
    }
}
