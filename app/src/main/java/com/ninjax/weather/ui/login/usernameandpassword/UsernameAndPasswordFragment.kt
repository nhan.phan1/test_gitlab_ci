package com.ninjax.weather.ui.login.usernameandpassword

import com.ninjax.weather.R
import com.ninjax.weather.ui.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * Created by nmduc3 on 5/29/20
 */
class UsernameAndPasswordFragment: BaseFragment<UsernameAndPasswordViewModel>() {
    override fun getLayoutResource() = R.layout.fragment_username_and_password

    override fun viewModel() = viewModel<UsernameAndPasswordViewModel>().value
}
