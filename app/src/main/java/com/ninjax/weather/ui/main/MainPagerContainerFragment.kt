package com.ninjax.weather.ui.main

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.ninjax.weather.R
import com.ninjax.weather.ui.base.BaseFragment
import com.ninjax.weather.ui.main.home.HomeFragment
import com.ninjax.weather.viewmodel.BaseViewModel

/**
 * Created by nmduc3 on 6/1/20
 */
class MainPagerContainerFragment : BaseFragment<BaseViewModel>() {
    companion object {
        private const val KEY_POSITION = "key_position"

        fun newInstance(position: Int) = MainPagerContainerFragment().apply {
            arguments = Bundle().apply {
                putInt(KEY_POSITION, position)
            }
        }
    }

    private var currentFragment: Fragment? = null

    override fun getLayoutResource() = R.layout.fragment_container_home_page

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (currentFragment == null) {
            currentFragment = when (arguments?.getInt(KEY_POSITION)) {
                EMainPager.HOME.ordinal -> {
                    HomeFragment.newInstance(EMainPager.HOME.ordinal)
                }
                EMainPager.SEARCH.ordinal -> {
                    HomeFragment.newInstance(EMainPager.SEARCH.ordinal)
                }
                EMainPager.FAVOURITES.ordinal -> {
                    HomeFragment.newInstance(EMainPager.FAVOURITES.ordinal)
                }
                EMainPager.CALENDAR.ordinal -> {
                    HomeFragment.newInstance(EMainPager.CALENDAR.ordinal)
                }
                else -> null
            }
            currentFragment?.also {
                replaceInContainer(it, isAddBackStack = false, isEnableAnim = false)
            }
        }
    }

    override fun viewModel() = BaseViewModel()
}
