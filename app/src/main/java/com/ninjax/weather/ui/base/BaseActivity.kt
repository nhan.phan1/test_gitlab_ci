package com.ninjax.weather.ui.base

import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import android.view.WindowManager
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.ninjax.weather.R
import com.ninjax.weather.extension.addFragment
import com.ninjax.weather.extension.getNavigationBarHeight
import com.ninjax.weather.extension.replaceFragment
import com.ninjax.weather.util.Constants

abstract class BaseActivity : AppCompatActivity() {

    private lateinit var mainContainer: BaseFragment<*>
    private var superContainer: BaseFragment<*>? = null

    abstract fun mainContainer(): BaseFragment<*>

    open fun superContainer(): BaseFragment<*>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initBase()
        // full screen
        window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        // set Content View
        createViewForActivity(savedInstanceState)
        // check navigation bar Back/Home/Multiple task
        checkNavigationBar()
        replaceFragment(mainContainer, isEnableAnim = false, addBackStack = false)
        superContainer?.let {
            addFragment(it, false)
        }
    }

    private fun initBase() {
        mainContainer = mainContainer()
        superContainer = superContainer()?.apply {
            setLevel(Constants.Level.CONTAINER)
        }
    }

    open fun createViewForActivity(savedInstanceState: Bundle?) {
        getLayoutResource()?.run { setContentView(this) }
    }

    @LayoutRes
    open fun getLayoutResource(): Int? = null

    private fun hasNavigationBar(): Boolean {
        val d = windowManager.defaultDisplay

        val realDisplayMetrics = DisplayMetrics()
        d.getRealMetrics(realDisplayMetrics)

        val realHeight = realDisplayMetrics.heightPixels

        val displayMetrics = DisplayMetrics()
        d.getMetrics(displayMetrics)

        val displayHeight = displayMetrics.heightPixels

        return realHeight - displayHeight > 0
    }

    private fun checkNavigationBar() {
        if (hasNavigationBar()) {
            //padding bottom if has navigation bar
            val parent = findViewById<View>(R.id.masterPage)
            parent?.let {
                it.setPadding(
                    it.paddingStart,
                    it.paddingTop,
                    it.paddingEnd,
                    getNavigationBarHeight()
                )
            }
        }
    }

    internal fun addSuperFragment(
        fragment: Fragment,
        isEnableAnim: Boolean = true, tagNameBackStack: String? = null
    ) {
        superContainer?.addInContainer(fragment, isEnableAnim, tagNameBackStack)
    }
}
