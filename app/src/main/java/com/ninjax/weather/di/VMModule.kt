package com.ninjax.weather.di

import com.ninjax.weather.ui.login.termofservice.TermOfServiceViewModel
import com.ninjax.weather.ui.login.usernameandpassword.UsernameAndPasswordViewModel
import com.ninjax.weather.ui.main.MainViewModel
import com.ninjax.weather.ui.main.home.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { MainViewModel() }
    viewModel { HomeViewModel() }
    viewModel { TermOfServiceViewModel() }
    viewModel { UsernameAndPasswordViewModel() }
}
