package com.ninjax.weather.util

class Constants {
    object Config {
        const val DATABASE = "weather_db"
    }

    object Util {
        const val PREFERENCES_KEY = "app_preference"
    }

    object Level {
        const val TOP = 0
        const val CONTAINER = 1
        const val TAB = 2
    }
}
