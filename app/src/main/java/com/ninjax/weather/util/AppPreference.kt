package com.ninjax.weather.util

import android.content.SharedPreferences
import androidx.core.content.edit
import org.koin.core.KoinComponent
import org.koin.core.inject

object AppPreference : KoinComponent {
    private val sharePref by inject<SharedPreferences>()

    private const val KEY_IS_LOGIN = "pref_key_is_login"

    var isLogin: Boolean
        get() = sharePref.getBoolean(KEY_IS_LOGIN, false)
        set(value) = sharePref.edit {
            putBoolean(KEY_IS_LOGIN, value)
        }
}
