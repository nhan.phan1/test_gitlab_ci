package com.ninjax.weather.extension

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.ninjax.weather.R

fun AppCompatActivity.replaceFragment(
    fragment: Fragment,
    isEnableAnim: Boolean = true,
    addBackStack: Boolean = true,
    tagNameBackStack: String? = null
) {
    supportFragmentManager.beginTransaction().apply {
        if (isEnableAnim) {
            setCustomAnimations(
                R.anim.slide_in_right, R.anim.slide_out_left,
                R.anim.slide_in_left, R.anim.slide_out_right
            )
        }
        replace(R.id.container, fragment, fragment.javaClass.simpleName)
        if (addBackStack) {
            addToBackStack(tagNameBackStack)
        }
        commit()
    }
}

fun AppCompatActivity.addFragment(
    fragment: Fragment,
    isEnableAnim: Boolean = true,
    tagNameBackStack: String? = null
) {
    supportFragmentManager.beginTransaction().apply {
        if (isEnableAnim) {
            setCustomAnimations(
                R.anim.slide_in_right, R.anim.slide_out_left,
                R.anim.slide_in_left, R.anim.slide_out_right
            )
        }
        add(R.id.container, fragment, fragment.javaClass.simpleName)
        addToBackStack(tagNameBackStack)
        commit()
    }
}
